import { Component, ElementRef, EventEmitter, Output, ViewChild, } from '@angular/core';
import { CarObject } from '../cars/cars.component'; 

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.scss']
})
export class AddCarComponent {
  
 
  @Output('onAddCar') carEmitter = new EventEmitter<CarObject>();
  @ViewChild('carYearInput') carYearInput!: ElementRef;

  constructor(){}

    addCar(carNameEl: HTMLInputElement){

this.carEmitter.emit({
  name: carNameEl.value,
  year: +this.carYearInput.nativeElement.value
})

      carNameEl.value = '';
      this.carYearInput.nativeElement.value = 2017;
      
    }
  

}
