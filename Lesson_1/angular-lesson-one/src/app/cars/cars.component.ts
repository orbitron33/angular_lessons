import { Component, ViewEncapsulation, OnChanges, OnInit } from '@angular/core';

export interface CarObject {
  name: string,
  year: number
}

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss'],

})
export class CarsComponent {

 
  cars: CarObject[] = [{
    name: 'Ford',
    year: 2015}
  //, {
  //   name: 'BMW',
  //   year: 2017
  // },{
  //   name: 'Audi',
  //   year: 2018
  // }
]

constructor(){}

  updateCarList(car: CarObject){
this.cars.push(car);
  
}  
changeCarNam(){
  this.cars[0].name = 'New car name'
}

deleteCar(){
  this.cars.splice(0,1);
}
}
