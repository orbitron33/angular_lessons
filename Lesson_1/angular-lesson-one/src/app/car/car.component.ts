import { Component, ContentChild, DoCheck, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit, OnChanges, DoCheck {
@Input('carItem') car!: {name: string, year: number};
@Input() name!: string;
@ContentChild('carHeading') carHeading!: ElementRef;
constructor(){}
ngOnInit(): void {
  
}
ngOnChanges(changes: SimpleChanges): void {
}
ngDoCheck() {}
}